//load library express
var express = require("express");
//load library path
var path = require("path");

//create an instance of express application
var app = express();

// serve files from public directory
// __dirname is the absolute path of the application directory
app.use(express.static(path.join(__dirname, "/../public/pages")));
app.use(express.static(path.join(__dirname, "/../public/js")));
//mask bower_components into lib directory
app.use("/lib", express.static(path.join(__dirname, "/../server/bower_components")));
    
// handle register requests
app.get("/register", function(req, resp) {

  var registrant = {
    email:req.query.email,
    password:req.query.password,
    name:req.query.name,
    gender:req.query.gender,
    dob:req.query.dob,
    address:req.query.address,
    country:req.query.country,
    contact:req.query.contact
  }

	console.log("registering ...");
  console.log(JSON.stringify(registrant));
  resp.json(registrant);
	resp.status(201).end();

});

//define the port that our app is going to listen to
app.set("port", parseInt(process.argv[2]) || 3000);

//start the server
app.listen(app.get("port"), function() {
  console.log("Application started at %s on %d", new Date(), app.get("port"));
});